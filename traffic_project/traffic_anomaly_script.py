from pathlib import Path
import pandas as pd
import numpy as np
import datetime as dt

#Извлекаем данные
p_str=r'\\Reports'
p_way=Path(p_str)
way_lst=list(p_way.glob('**\*.zip'))
df_prv=pd.read_csv(way_lst[-2],compression='zip',sep=';',encoding='utf-16',skiprows=[0,1,3],usecols=['Direction','Partner TAP Code','Service Family','Number of Subscribers','Chargeable Megabytes','Chargeable Minutes','Number of CallEventDetails'])
df_now=pd.read_csv(way_lst[-1],compression='zip',sep=';',encoding='utf-16',skiprows=[0,1,3],usecols=['Direction','Partner TAP Code','Service Family','Number of Subscribers','Chargeable Megabytes','Chargeable Minutes','Number of CallEventDetails'])

#Очистка данных

def data_clean(data):
    data.columns=data.columns.str.replace(' ','_').str.replace('.','_').str.lower()
    columns=data.columns
    for c in columns[3:]:
        data[c]=data[c].str.replace(',','').astype(float)
    data=data.fillna(0)
    return data
    
df_prv=data_clean(df_prv)
df_now=data_clean(df_now)

#Группировка и объединение данных
def group_df_func(df,subscriber_type):
    grouped_df=df.query('direction==@subscriber_type') \
            .groupby(['partner_tap_code','service_family'],as_index=False) \
            .agg({'number_of_subscribers':'sum','chargeable_megabytes':'sum','chargeable_minutes':'sum','number_of_calleventdetails':'sum'}) \
            .set_index('partner_tap_code')
    return grouped_df
    
df_prv_own=group_df_func(df_prv,'Own Subscribers')
df_prv_visit=group_df_func(df_prv,'Visiting Subscribers')
df_now_visit=group_df_func(df_now,'Visiting Subscribers')
df_now_own=group_df_func(df_now,'Own Subscribers')

def merge_df_func(df1,df2):
    full_data=df1.merge(df2,how='outer',on=['partner_tap_code','service_family'],suffixes=("_prv","_now")).fillna(0)
    return full_data
   
own_merged=merge_df_func(df_prv_own,df_now_own)
visit_merged=merge_df_func(df_prv_visit,df_now_visit)

#дополнительный столбец для фильтрации по числу абонентов
own_merged['avg_customer_numb']=(own_merged.number_of_subscribers_now + own_merged.number_of_subscribers_prv)/2
visit_merged['avg_customer_numb']=(visit_merged.number_of_subscribers_now + visit_merged.number_of_subscribers_prv)/2

#Относительный прирост трафика. Единица в знаменателе добавляется для избежания деления на 0.
def delta(column1,column2):
    return (100*(column2- column1)/(column1+1)).round(2)
    
#вычисление прироста для всех типов трафика

own_merged['dMin']=delta(own_merged['chargeable_minutes_prv'],own_merged['chargeable_minutes_now'])
own_merged['dMb']=delta(own_merged['chargeable_megabytes_prv'], own_merged['chargeable_megabytes_now'])
own_merged['dSMS']=delta(own_merged['number_of_calleventdetails_prv'],own_merged['number_of_calleventdetails_now'])
visit_merged['dMin']=delta(visit_merged['chargeable_minutes_prv'],visit_merged['chargeable_minutes_now'])
visit_merged['dMb']=delta(visit_merged['chargeable_megabytes_prv'], visit_merged['chargeable_megabytes_now'])
visit_merged['dSMS']=delta(visit_merged['number_of_calleventdetails_prv'],visit_merged['number_of_calleventdetails_now'])

#Фильтрация по пороговому значению

mb_own=own_merged \
                    .query('avg_customer_numb>=5 and service_family=="GPRS" and (dMb>60 or dMb<-60 or dMb==0)') \
                    [['number_of_subscribers_prv','number_of_subscribers_now','chargeable_megabytes_prv','chargeable_megabytes_now','dMb']] \
                    .sort_values('dMb',ascending=False)
mb_visit=visit_merged \
                    .query('avg_customer_numb>=5 and service_family=="GPRS" and (dMb>60 or dMb<-60 or dMb==0)') \
                    [['number_of_subscribers_prv','number_of_subscribers_now','chargeable_megabytes_prv','chargeable_megabytes_now','dMb']] \
                    .sort_values('dMb',ascending=False)
                    

sms_own=own_merged \
                    .query('avg_customer_numb>=5 and service_family=="SMS" and (dSMS>60 or dSMS<-60 or dSMS==0)') \
                    [['number_of_subscribers_prv','number_of_subscribers_now','number_of_calleventdetails_prv','number_of_calleventdetails_now','dSMS']] \
                    .sort_values('dSMS',ascending=False)
sms_visit=visit_merged \
                    .query('avg_customer_numb>=5 and service_family=="SMS" and (dSMS>60 or dSMS<-60 or dSMS==0)') \
                    [['number_of_subscribers_prv','number_of_subscribers_now','number_of_calleventdetails_prv','number_of_calleventdetails_now','dSMS']] \
                    .sort_values('dSMS',ascending=False)
                    
                    
minutes_own=own_merged \
                .query('avg_customer_numb>=5 and service_family=="Teleservice" and (dMin>60 or dSMS<-60 or dMin==0)') \
                [['number_of_subscribers_prv','number_of_subscribers_now','chargeable_minutes_prv','chargeable_minutes_now','dMin']] \
                .sort_values('dMin',ascending=False)
minutes_visit=visit_merged \
                .query('avg_customer_numb>=5 and service_family=="Teleservice" and (dMin>60 or dSMS<-60 or dMin==0)') \
                [['number_of_subscribers_prv','number_of_subscribers_now','chargeable_minutes_prv','chargeable_minutes_now','dMin']] \
                .sort_values('dMin',ascending=False)
        
#Сохранение отчета        
filename = r'\\Reports\Traffic_report_' + str(dt.datetime.now().strftime("%Y%m%d")) + '.xlsx'

with pd.ExcelWriter(filename) as writer:
    sms_own.to_excel(writer,sheet_name='dSMS_own')
    mb_own.to_excel(writer,sheet_name='dMb_own')
    minutes_own.to_excel(writer,sheet_name='dMin_own')
    sms_visit.to_excel(writer,sheet_name='dSMS_visit')
    mb_visit.to_excel(writer,sheet_name='dMb_visit')
    minutes_visit.to_excel(writer,sheet_name='dMin_visit')
