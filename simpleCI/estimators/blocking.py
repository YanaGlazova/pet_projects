from __future__ import division
import numpy as np

from .base import Estimator


class Blocking(Estimator):

	"""
	Dictionary-like class containing treatment effect estimates.
	"""

	def __init__(self, strata, adj):
	
		self._method = 'Blocking'
		for s in strata:
			s.est_via_ols(adj)

		Ns = [s.raw_data['N'] for s in strata]
		N_cs = [s.raw_data['N_c'] for s in strata]
		N_ts = [s.raw_data['N_t'] for s in strata]

		ates = [s.estimates['ols']['ate'] for s in strata]
		ate_ses = [s.estimates['ols']['ate_se'] for s in strata]
		if adj <= 1:
			atcs, atts = ates, ates
			atc_ses, att_ses = ate_ses, ate_ses
		else:
			atcs = [s.estimates['ols']['atc'] for s in strata]
			atts = [s.estimates['ols']['att'] for s in strata]
			atc_ses = [s.estimates['ols']['atc_se'] for s in strata]
			att_ses = [s.estimates['ols']['att_se'] for s in strata]

		self._dict = dict()
		attse, atcse, atese = [], [], []
		att_s, atc_s, ate_s = [], [], []
		for i in range(s.raw_data['Y'].shape[1]):

			ate = calc_atx(ates[i], Ns)
			atc = calc_atx(atcs[i], N_cs)
			att = calc_atx(atts[i], N_ts)

			ate_se = calc_atx_se(ate_ses[i], Ns)
			atc_se = calc_atx_se(atc_ses[i], N_cs)
			att_se = calc_atx_se(att_ses[i], N_ts)
			ate_s.append(ate)
			atc_s.append(atc)
			att_s.append(att)
			atcse.append(atc_se)
			attse.append(att_se)
			atese.append(ate_se)

		self._dict['ate'] = ate_s
		self._dict['atc'] = atc_s
		self._dict['att'] = att_s
		self._dict['ate_se'] = atese
		self._dict['atc_se'] = atcse
		self._dict['att_se'] = attse


def calc_atx(atxs, Ns):

	N = sum(Ns)

	return np.sum(np.array(atxs) * np.array(Ns)) / N


def calc_atx_se(atx_ses, Ns):

	N = sum(Ns)
	var = np.sum(np.array(atx_ses)**2 * np.array(Ns)**2) / N**2

	return np.sqrt(var)

